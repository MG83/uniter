![Release](https://jitpack.io/v/org.bitbucket.MG83/uniter.svg?style=flat-square)
https://jitpack.io/#org.bitbucket.MG83/uniter

# Uniter Mvvm
super easy to use android/kotlin framework for mvvm programming pattern. Reduce boilerplate code, remove android lifecycle complexity   

## Getting started

### gradle
project.gradle 
```
maven { url 'https://jitpack.io' }
```

app.gradle 
```
implementation 'org.bitbucket.MG83:uniter:{latest-version}'
```
enable databinding
````
android{
    ...
    dataBinding { enabled true }
    ...
````

#### prepare android studio
install intellij plugin https://github.com/shiraji/databinding-support


#### install uniter templates 
speed up your workflow and reduce the boilerplate code

1. copy the files from  `uniter/templates`  to  `AndroidStudio/Contents/plugins/android/lib/templates/uniter`
2. restart Android Studio
3. select your root package and press command+n -> select uniter/activity
4. code the fun part

## ....

## License

```
Copyright 2017 Marian Gieseler

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

