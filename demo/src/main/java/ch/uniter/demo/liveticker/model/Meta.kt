package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Meta(
        @SerializedName("pagination") @Expose var pagination: Pagination? = null,
        @SerializedName("sport") @Expose var sport: Sport? = null,
        @SerializedName("plan") @Expose var plan: Plan? = null,
        @SerializedName("subscription") @Expose var subscription: Subscription? = null
)
