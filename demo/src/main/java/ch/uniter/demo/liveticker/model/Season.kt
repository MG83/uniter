package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

class Season(val id: Int,
             val name: String,
             @SerializedName("league_id") val leagueId: Int,
             @SerializedName("is_current_season") val isCurrentSeason: Boolean,
             @SerializedName("current_round_id") val currentRoundId: String? = null,
             @SerializedName("current_stage_id") val currentStageId: String? = null

)