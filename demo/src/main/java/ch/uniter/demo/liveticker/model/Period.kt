package ch.uniter.demo.liveticker.model

class Period(val count:String, val minute:String, val percentage:String)
class PeriodData(val period:List<Period>)