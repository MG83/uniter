package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

class Formation (
    @SerializedName("localteam_formation") val localteamFormation:String,
    @SerializedName("visitorteam_formation") val visitorteamFormation:String
)