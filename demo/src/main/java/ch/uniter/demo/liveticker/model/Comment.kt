package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

class Comment (
    @SerializedName("fixture_id") val fixtureId:Int,
    val important:Boolean,
    val order:Int,
    val goal:Boolean,
    val minute:Int,
    @SerializedName("extra_minute") val extraMinute:Int? = null,
    val comment:String? = null
)