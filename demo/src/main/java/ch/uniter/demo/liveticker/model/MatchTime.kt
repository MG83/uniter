package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

data class MatchTime(
        val status: String,
        @SerializedName("starting_at") val startingAt: Time,
        @SerializedName("minute") val minute:Int,
        @SerializedName("extra_minute") val extraMinute:Int? = null,
        @SerializedName("injury_time") val injuryTime:Int? = null
)

data class Time(
    @SerializedName("date_time") val dateTime:String,
    val date:String,
    val timestamp:Long,
    val timezone:String
)
