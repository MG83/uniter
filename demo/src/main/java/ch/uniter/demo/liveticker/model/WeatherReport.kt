package ch.uniter.demo.liveticker.model

class WeatherReport(
        val code: String? = null,
        val type: String? = null,
        val icon: String? = null,
        val temperature: Temperature? = null,
        val clouds: String? = null,
        val humidity: String? = null,
        val wind: Wind? = null
)

class Temperature(
    val temp: Float,
    val unit: String
)

class Wind(
    val speed: String,
    val degree: Int
)