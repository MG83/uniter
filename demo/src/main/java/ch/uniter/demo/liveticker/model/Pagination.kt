package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Pagination(
    @SerializedName("links") @Expose val links: Any? = null,
    @SerializedName("total_pages") @Expose val totalPages: Int? = null,
    @SerializedName("current_page") @Expose val currentPage: Int? = null,
    @SerializedName("per_page") @Expose val perPage: Int? = null,
    @SerializedName("count") @Expose val count: Int? = null,
    @SerializedName("total") @Expose val total: Int? = null
)