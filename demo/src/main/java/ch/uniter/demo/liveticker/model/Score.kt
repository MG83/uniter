package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

class Score (
  @SerializedName("localteam_score") val localteamScore:Int? = null,
  @SerializedName("visitorteam_score") val visitorteamScore:Int? = null,
  @SerializedName("localteam_pen_score") val localteamPenScore:Any? = null,
  @SerializedName("visitorteam_pen_score") val visitorteamPenScore:Any? = null,
  @SerializedName("ht_score") val htScore:String? = null,
  @SerializedName("ft_score") val ftScore:String? = null,
  @SerializedName("et_score") val etScore:String? = null
)