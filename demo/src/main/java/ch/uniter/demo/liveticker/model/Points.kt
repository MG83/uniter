package ch.uniter.demo.liveticker.model

data class Points( val away:Int, val home:Int, val total:Int)