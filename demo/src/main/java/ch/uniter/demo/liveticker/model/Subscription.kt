package ch.uniter.demo.liveticker.model

import ch.uniter.demo.liveticker.model.StartedAt
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Subscription(
        @SerializedName("started_at") @Expose var startedAt: StartedAt? = null,
        @SerializedName("trial_ends_at") @Expose var trialEndsAt: Any? = null,
        @SerializedName("ends_at") @Expose var endsAt: Any? = null
)

