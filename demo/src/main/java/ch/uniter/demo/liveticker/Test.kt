package ch.uniter.demo.liveticker

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by YooApps on 16.08.17.
 */
class Test {
    fun testContinents(){
        val response = SportMonksService.api.continents().execute()
        if (response.isSuccessful) {
            println("Continents -->")
            val data = response.body()!!.data
            for (d in data!!){
                println("continent:${d.name}")
            }
            println("<-- Continents")
            testCountries()
        }else{
            println(response.errorBody())
        }
    }

    fun testCountries(){
        val response = SportMonksService.api.countries().execute()
        if (response.isSuccessful) {
            println("Countries -->")
            val data = response.body()!!.data
            for (d in data!!){
                println("country:${d.name}")
            }
            println("<-- Countries")
            testLeagues()
        }else{
            println(response.errorBody())
        }
    }

    fun testLeagues(){
        val response = SportMonksService.api.leagues().execute()
        if (response.isSuccessful) {
            println("Leagues -->")
            val data = response.body()!!.data
            for (d in data!!){
                println("league:${d.name}")
            }
            println("<-- Leagues")
            testSeason()
        }else{
            println(response.errorBody())
        }
    }

    fun testSeason(){
        val response = SportMonksService.api.seasons().execute()
        if (response.isSuccessful) {
            println("Seasons -->")
            val data = response.body()!!.data
            var currentOrLastSeason = data!!.last().id
            for (d in data){
                println("season:${d.name}")
                if(d.isCurrentSeason)
                    currentOrLastSeason = d.id
            }
            println("<-- Seasons")
            testTeams(currentOrLastSeason)
        }else{
            println(response.errorBody())
        }
    }

    fun testTeams(seasonId: Int){
        val response = SportMonksService.api.teamsBySeasonId(seasonId).execute()
        if (response.isSuccessful) {
            println("Teams -->")
            val data = response.body()!!.data
            for (d in data!!){
                println("teamName:${d.name}")
            }
            println("<-- Teams")
        }else{
            println(response.errorBody())
        }
    }

    fun testFixtures(from: Date = Date()){

        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val date = formatter.format(from)

        println("get fixtures for date:$date")

        val response = SportMonksService.api.fixtureFrom(date).execute()
        if (response.isSuccessful) {
            println("Fixtures -->")
            val data = response.body()!!.data!!
            for (d in data){
                println("match:${d.visitorteamId}")
            }
            println("<--Fixtures")
            val match = data.first()
            testHead2Head(match.localteamId,match.visitorteamId)
        }else{
            println(response.errorBody())
        }
    }

    fun testHead2Head(teamId1:Int, teamId2:Int){
        val response = SportMonksService.api.head2head(teamId1,teamId2).execute()
        if (response.isSuccessful) {
            println("Head2Head -->")
            val data = response.body()!!.data
            for (d in data!!){
                println("etScore:${d.scores?.etScore}")
                println("ftScore:${d.scores?.ftScore}")
                println("htScore:${d.scores?.htScore}")
            }
            println("<-- Head2Head")
        }else{
            println(response.errorBody())
        }
    }

}