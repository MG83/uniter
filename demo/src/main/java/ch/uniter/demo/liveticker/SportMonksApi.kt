package ch.uniter.demo.liveticker

import ch.uniter.demo.liveticker.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SportMonksApi {

    @GET("continents")
    fun continents(): Call<Response<Continent>>

    @GET("countries")
    fun countries(): Call<Response<Country>>

    @GET("seasons")
    fun seasons(): Call<Response<Season>>

    @GET("leagues")
    fun leagues(): Call<Response<League>>

    @GET("leagues/{leagueId}")
    fun leagueById(@Path("leagueId") leagueId: Int?): Call<SingleResponse<League>>

    @GET("fixtures/date/{from}")
    fun fixtureFrom(@Path("from") from: String): Call<Response<Match>>

    @GET("fixtures/between/{from}/{to}")
    fun fixtureBetween(@Path("from") from: String, @Path("to") to: String, @Query("include") include:String = "localTeam,visitorTeam,events" ): Call<Response<Match>>

    @GET("fixtures/{fixtureId}")
    fun fixtureById(@Path("fixtureId") fixtureId: Int?): Call<SingleResponse<Match>>

    @GET("livescores")
    fun livescores(): Call<Response<Match>>

    @GET("livescores/now")
    fun livescoresNow(): Call<Response<Match>>

    @GET("commentaries/fixture/{fixtureId}")
    fun commentaries(@Path("fixtureId") fixtureId: Int?): Call<Response<Comment>>

    @GET("highlights")
    fun highlights(): Call<Response<Highlite>>

    @GET("highlights/{fixtureId}")
    fun highlights(@Path("fixtureId") fixtureId: Int?): Call<Response<Highlite>>

    @GET("head2head/{teamId1}/{teamId2}")
    fun head2head(@Path("teamId1") teamId1: Int?, @Path("teamId2") teamId2: Int?): Call<Response<Match>>

    @GET("teams/{teamId}")
    fun team(@Path("teamId") teamId: Int?): Call<SingleResponse<Team>>

    @GET("teams/season/{id}")
    fun teamsBySeasonId(@Path("id") seasonId: Int?): Call<Response<Team>>

    @GET("teams/{teamId}&include=stats")
    fun teamWithStats(@Path("teamId") teamId: Int?): Call<SingleResponse<TeamWithStats>>
}
