package ch.uniter.demo.liveticker.model

data class Continent(val id: Int, val name: String)