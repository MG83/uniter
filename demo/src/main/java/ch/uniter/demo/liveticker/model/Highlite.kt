package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

data class Highlite (
    @SerializedName("fixture_id") val fixtureId:Int,
    val location:String,
    @SerializedName("created_at") val createdAt: Time
)