package ch.uniter.demo.liveticker.model

class Stats (
        val team_id:Int,
        val season_id:Int,
        val win: Points?=null,
        val draw: Points?=null,
        val lost: Points?=null,
        val goals_for: Points?=null,
        val goals_against: Points?=null,
        val clean_sheet: Points?=null,
        val scoring_minutes:List<PeriodData>?=null,
        val avg_goals_per_game_scored:List<Points>?=null,
        val avg_goals_per_game_conceded:List<Points>?=null,
        val avg_first_goal_scored:List<Points>?=null,
        val avg_first_goal_conceded:List<Points>?=null,
        val failed_to_score:List<Points>?=null
        )

class StatsData(val data:List<Stats>?=null)