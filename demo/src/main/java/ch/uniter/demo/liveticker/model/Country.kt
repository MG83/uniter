package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

class Country (val id: Int, val name:String, val extra: CountryExtra)
class CountryExtra(
    val continent: String,
    @SerializedName("sub_region") val subRegion: String,
    @SerializedName("world_region") val worldRegion: String,
    val fifa: String,
    val longitude:Double,
    val latitude: Double,
    val flag: String
 )