package ch.uniter.demo.liveticker.model

/**
 * Created by YooApps on 16.08.17.
 */

data class ErrorBody(val error:Error?)
data class Error (val message:String = "...", val code:Int = -1)