package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

open class Team (
    val id: Int,
    val name: String,
    val twitter: String? = null,
    @SerializedName("country_id") val countryId: Int,
    @SerializedName("national_team") val nationalTeam: Boolean,
    val founded: Int,
    @SerializedName("logo_path") val logoPath: String,
    @SerializedName("venue_id") val venueId: Int
)