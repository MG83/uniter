package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Plan(
    @SerializedName("request_limit") @Expose val requestLimit: String? = null,
    @SerializedName("price") @Expose val price: String? = null,
    @SerializedName("name") @Expose val name: String? = null
)
