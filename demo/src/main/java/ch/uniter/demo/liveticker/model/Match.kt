package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Match(
        val id: Int,
        @SerializedName("league_id") val leagueId:Int,
        @SerializedName("season_id") val seasonId:Int,
        @SerializedName("stage_id") val stageId:Int,
        @SerializedName("round_id") val roundId:Int,
        @SerializedName("aggregate_id") val aggregateId:Int? = null,
        @SerializedName("venue_id") val venueId:Int? = null,
        @SerializedName("referee_id") val refereeId:Int? = null,
        @SerializedName("localteam_id") val localteamId:Int,
        @SerializedName("visitorteam_id") val visitorteamId:Int,
        @SerializedName("weather_report") val weatherReport: WeatherReport? = null,
        val commentaries:Boolean = false,
        val attendance: Formation? = null,
        @SerializedName("winning_odds_calculated") val winningOddsCalculated:Boolean,
        val formations:Any? = null,
        val scores: Score? = null,
        val time: MatchTime? = null,
        val coaches: Coaches? = null,
        val standings:Any? = null,
        val deleted:Boolean = false,
        @SerializedName("localTeam")val localTeamWrapper: SingleResponse<Team>? = null,
        @SerializedName("visitorTeam")val visitorTeamWrapper: SingleResponse<Team>? = null,
        @Expose var stats: String = "-:-"
){
    @Expose  @SerializedName("_localTeam") val localTeam: Team? = localTeamWrapper?.data
    @Expose  @SerializedName("_visitorTeam") val visitorTeam: Team? = visitorTeamWrapper?.data
}