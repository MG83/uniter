package ch.uniter.demo.liveticker

import android.util.Log
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

object SportMonksService {

    private const val API_BASE_URL = "https://soccer.sportmonks.com/api/v2.0/"
    private const val ENABLE_REST_LOG = true

    val api:SportMonksApi by lazy {builder().build().create(SportMonksApi::class.java)}

    private fun builder(): Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient().build())
    }

    private class LoggingInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            val response = chain.proceed(request)
            if (ENABLE_REST_LOG) {
                val t1 = System.nanoTime()
                Log.v("rest", String.format("Sending request %s on %s%n%s",
                        request.url, chain.connection(), request.headers))

                val t2 = System.nanoTime()
                Log.v("rest", String.format("Received response for %s in %.1fms%n%s",
                        response.request.url, (t2 - t1) / 1e6, response.headers))
            }

            return response
        }
    }

    fun getClient(): OkHttpClient.Builder {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url

            val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("api_token", "j2yceUXPw4qeaKRBHHZVnYLB8Twy6aRn7xtr8uMJHzXHa9Nf6Qom3VPBokji")
                    .build()

            val requestBuilder = original.newBuilder()
                    .url(url)

            val request = requestBuilder.build()
            chain.proceed(request)
        }

        httpClient.addInterceptor { chain: Interceptor.Chain ->
            val original = chain.request()

            val request = original.newBuilder()
                    .header("Accept", "application/json")
                    .method(original.method, original.body)
                    .build()

            chain.proceed(request)
        }

        return httpClient
    }
}
