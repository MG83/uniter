package ch.uniter.demo.liveticker

import java.util.*

/**
 * Created by YooApps on 17.08.17.
 */

fun Date.addDays(days:Int):Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(Calendar.DAY_OF_YEAR, days)
    return calendar.time
}



