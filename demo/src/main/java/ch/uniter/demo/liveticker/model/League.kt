package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

data class League(
    val id:Int,
    @SerializedName("legacy_id") val legacyId:Int,
    @SerializedName("country_id") val countryId:Int,
    val name:String,
    @SerializedName("is_cup") val isCup: Boolean,
    @SerializedName("current_season_id") val currentSeasonId: Int,
    @SerializedName("current_round_id") val currentRoundId: Int,
    @SerializedName("current_stage_id") val currentStageId: Int,
    @SerializedName("live_standings") val liveStandings: Boolean
)