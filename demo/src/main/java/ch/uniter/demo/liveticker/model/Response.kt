package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.Expose

data class Response<T>(@Expose var data: List<T>? = null, @Expose var meta: Meta? = null)
data class SingleResponse<T>(@Expose var data: T? = null, @Expose var meta: Meta? = null)
