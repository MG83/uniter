package ch.uniter.demo.liveticker.model

import com.google.gson.annotations.SerializedName

class Coaches (
    @SerializedName("localteam_coach_id") val localteamCoachId:Int? = null,
    @SerializedName("visitorteam_coach_id") val visitorteamCoachId:Int? = null
)