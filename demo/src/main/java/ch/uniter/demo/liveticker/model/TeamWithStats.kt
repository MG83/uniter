package ch.uniter.demo.liveticker.model

import ch.uniter.demo.liveticker.model.StatsData
import ch.uniter.demo.liveticker.model.Team

class TeamWithStats(id: Int,
                    name: String,
                    twitter: String?,
                    countryId: Int,
                    nationalTeam: Boolean,
                    founded: Int,
                    logoPath: String,
                    venueId: Int,
                    val stats: StatsData?=null)
    : Team(id, name, twitter, countryId, nationalTeam, founded, logoPath, venueId)
