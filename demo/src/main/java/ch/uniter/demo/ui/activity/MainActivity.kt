package ch.uniter.demo.ui.activity

import android.os.Bundle
import ch.uniter.demo.R
import ch.uniter.demo.databinding.ActivityMainBinding
import ch.uniter.demo.ui.fragment.MatchListFragment
import ch.uniter.demo.vm.MainViewModel
import ch.uniter.mvvm.MvvmActivity
import ch.uniter.mvvm.annotation.MvvmLayout


@MvvmLayout(R.layout.activity_main)
class MainActivity : MvvmActivity<ActivityMainBinding, MainViewModel>() {
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        if(savedInstanceState==null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, MatchListFragment.newInstance(), "match_list")
                    .commit()
        }
    }
}
