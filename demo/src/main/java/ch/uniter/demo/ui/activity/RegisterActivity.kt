package ch.uniter.demo.ui.activity

import android.os.Bundle
import ch.uniter.demo.R
import ch.uniter.demo.databinding.ActivityRegisterBinding
import ch.uniter.demo.vm.LoginViewModel
import ch.uniter.mvvm.MvvmActivity

class RegisterActivity : MvvmActivity<ActivityRegisterBinding, LoginViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setup(R.layout.activity_register, true)
        super.onCreate(savedInstanceState)
    }
}
