package ch.uniter.demo.ui.fragment

import ch.uniter.demo.R
import ch.uniter.demo.databinding.FragmentMatchListBinding
import ch.uniter.demo.vm.MainViewModel
import ch.uniter.mvvm.MvvmFragment
import ch.uniter.mvvm.annotation.MvvmLayout

@MvvmLayout(R.layout.fragment_match_list, isShared=true)
class MatchListFragment : MvvmFragment<FragmentMatchListBinding, MainViewModel>() {
    companion object {
        fun newInstance():MatchListFragment {
            val fragment = MatchListFragment()
            return fragment
        }
    }
}
