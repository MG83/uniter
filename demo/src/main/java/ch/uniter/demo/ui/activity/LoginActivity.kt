package ch.uniter.demo.ui.activity

import ch.uniter.demo.R
import ch.uniter.demo.databinding.ActivityLoginBinding
import ch.uniter.demo.vm.LoginViewModel
import ch.uniter.mvvm.MvvmActivity
import ch.uniter.mvvm.annotation.MvvmLayout

/**
 * Created by gim on 03.10.17.
 */

@MvvmLayout(R.layout.activity_login,isShared = true)
class LoginActivity : MvvmActivity<ActivityLoginBinding, LoginViewModel>()