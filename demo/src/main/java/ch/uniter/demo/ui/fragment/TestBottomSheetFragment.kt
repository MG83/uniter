package ch.uniter.demo.ui.fragment

import ch.uniter.mvvm.annotation.MvvmLayout

import ch.uniter.demo.R
import ch.uniter.demo.databinding.FragmentTestBottomSheetBinding
import ch.uniter.demo.vm.MainViewModel
import ch.uniter.mvvm.MvvmBottomSheetDialogFragment


@MvvmLayout(R.layout.fragment_test_bottom_sheet, true)
class TestBottomSheetFragment : MvvmBottomSheetDialogFragment<FragmentTestBottomSheetBinding, MainViewModel>()