package ch.uniter.demo.binding

import android.widget.TextView
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter

/**
 * Created by gim on 05.11.17.
 */


object CustomBinding{
    @JvmStatic
    @BindingAdapter("android:text")
    fun setTextResource(view: TextView, @StringRes resource: Int) {
        view.setText(resource)
    }

}
