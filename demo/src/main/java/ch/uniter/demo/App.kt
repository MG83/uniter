package ch.uniter.demo

import android.app.Application
import kotlin.properties.Delegates

/**
 * Created by gim on 12.11.17.
 */
class App : Application() {

    companion object {
        var instance: App by Delegates.notNull()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}