package ch.uniter.demo.vm

import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import ch.uniter.demo.ui.activity.RegisterActivity
import ch.uniter.mvvm.MvvmActivity
import ch.uniter.mvvm.MvvmViewModel
import ch.uniter.validation.Validator

/**
 * Created by gim on 03.10.17.
 */
class LoginViewModel : MvvmViewModel() {

    val username = ObservableField<String>()
    val email = ObservableField<String>()
    val password = ObservableField<String>()

    private val validator: Validator by lazy { Validator( view!!.binding ) }

    fun validate(){
        validator.validate()
    }

    fun login(){
        Log.d("test","username:${username.get()} email:${email.get()} password:${password.get()}")
        val valid = validator.validate()
        Log.d("test","valid:$valid")

        //if(valid){
            activity!!.startActivity(Intent(activity, RegisterActivity::class.java))
       // }
    }

    fun destroy(){
        val a = view!!.mvvmActivity as MvvmActivity<*,*>

    }
}