package ch.uniter.demo.vm

import android.Manifest
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.RecyclerView
import ch.uniter.extension.logD
import ch.uniter.demo.BR
import ch.uniter.demo.R
import ch.uniter.demo.liveticker.SportMonksService
import ch.uniter.demo.liveticker.addDays
import ch.uniter.demo.liveticker.model.*
import ch.uniter.demo.ui.activity.LoginActivity
import ch.uniter.demo.ui.fragment.TestBottomSheetFragment
import ch.uniter.mvvm.MvvmViewModel
import ch.uniter.mvvm.threading.async
import ch.uniter.mvvm.threading.uiThread
import com.google.gson.Gson
import me.tatarka.bindingcollectionadapter2.BindingRecyclerViewAdapter
import me.tatarka.bindingcollectionadapter2.ItemBinding
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import java.net.SocketTimeoutException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Marian on 16.08.17.
 */
class MainViewModel : MvvmViewModel() {
    val hello = ObservableField(R.string.hello_blank_fragment)
    val isLoading = ObservableField<Boolean>()
    val leagues = ObservableArrayList<League>()
    val teams = ObservableArrayList<Team>()
    val seasons = ObservableArrayList<Season>()

    val fixtures = ObservableArrayList<Match>()
    val fixtureItem = ItemBinding.of<Match>(BR.item, R.layout.fixture_item)!!

    val selectedMatch=  ObservableField<Match>()
    val viewHolder = BindingRecyclerViewAdapter.ViewHolderFactory { binding -> MatchViewHolder(binding.root) }

    override fun onViewModelCreated() {
        Log.d("test","init vm -> id:"+ java.lang.System.identityHashCode(this))
        loadData()
    }

    override fun onViewAttached(firstAttachment: Boolean) {
        super.onViewAttached(firstAttachment)
     //   activity!!.startActivity(Intent(activity!!,LoginActivity::class.java))
        requestPermissions()
    }

    private fun requestPermissions(){
        permission.requestPermissions(arrayOf( Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_CONTACTS
        )
        ) { result ->

            Log.d("test", "all permission granted:${result.isGranted}")

            for (p in result.resultsMap) {
                Log.d("test", "permission:${p.key} isGranted:${p.value}")
            }

            if (result.isGranted) {

                // RUN CODE

            } else {
                requestPermissions()
            }
        }
    }

    var dialog:TestBottomSheetFragment? = null
    fun openBottomSheet(){
       // if(dialog==null)dialog = TestBottomSheetFragment()
       // dialog!!.show(activity?.supportFragmentManager, dialog!!.tag)

        activity!!.startActivity(Intent(activity,LoginActivity::class.java))
    }

    inner class MatchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { _ ->
                Log.d("item", "click:$bindingAdapterPosition")

                val match = fixtures[bindingAdapterPosition]
                selectedMatch.set(match)
            }
        }
    }

    private fun loadData(){
        async {
            Thread.sleep(6000)
            uiThread {
                logD("run async ok")
            }
        }
        isLoading.set(true)
      //  loadLeagues()
      //  loadSeasons()
        loadFixtureBetween(Date().addDays(-2),Date().addDays(7))
    }

    private fun loadSeasons(){
        val req = SportMonksService.api.seasons()
        req.enqueue(object:Callback<Response<Season>>{
            override fun onResponse(call: Call<Response<Season>>, response: retrofit2.Response<Response<Season>>) {
                if (response.isSuccessful) {
                    seasons.clear()
                    val data = response.body()!!.data
                    var currentOrLastSeason = data!!.last().id
                    for (d in data){
                        seasons.add(d)
                        if(d.isCurrentSeason)
                            currentOrLastSeason = d.id
                    }

                    loadTeams(currentOrLastSeason)

                }else{
                    handleError(response.errorBody()!!)
                }
            }
            override fun onFailure(call: Call<Response<Season>>?, t: Throwable?) {
                handleFailure(t)
            }

        })
    }

    private fun loadLeagues(){
        val req = SportMonksService.api.leagues()
        req.enqueue(object : Callback<Response<League>> {
            override fun onResponse(call: Call<Response<League>>?, response: retrofit2.Response<Response<League>>) {
                if (response.isSuccessful) {
                    leagues.clear()
                    leagues += response.body()!!.data!!

                }else{
                    handleError(response.errorBody()!!)
                }
            }

            override fun onFailure(call: Call<Response<League>>?, t: Throwable?) {
                handleFailure(t)
            }
        })
    }

    private fun loadTeams(seasonId:Int = 7953){
        val req = SportMonksService.api.teamsBySeasonId(seasonId)

        req.enqueue(object : Callback<Response<Team>>{
            override fun onResponse(call: Call<Response<Team>>?, response: retrofit2.Response<Response<Team>>) {
                if (response.isSuccessful) {
                    teams.clear()
                    teams += response.body()!!.data!!

                    loadFixtureBetween(Date().addDays(-2),Date().addDays(7))
                }else{
                    handleError(response.errorBody()!!)
                }
            }
            override fun onFailure(call: Call<Response<Team>>?, t: Throwable?) {
                handleFailure(t)
            }

        })
    }

    private fun loadFixtureBetween(fromDate:Date,toDate:Date){
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val from = formatter.format(fromDate)
        val to = formatter.format(toDate)
        val req = SportMonksService.api.fixtureBetween(from,to)
        req.enqueue(object:Callback<Response<Match>>{
            override fun onResponse(call: Call<Response<Match>>?, response: retrofit2.Response<Response<Match>>) {
               if(response.isSuccessful){
                   fixtures.clear()
                   for (fix in response.body()!!.data!!){
                      // if(fix.localTeam == null)
                     //  fix.localTeam = teams.firstOrNull { it.id == fix.localteamId }
                     //  fix.visitorTeam = teams.firstOrNull { it.id == fix.visitorteamId }

                       if(fix.scores!=null){
                           fix.stats = "${fix.scores.localteamScore} : ${fix.scores.visitorteamScore}"
                       }
                       fixtures.add(fix)
                   }
                   Log.d("DONE","done -> count:"+fixtures.size)
               }else{
                   handleError(response.errorBody()!!)
               }
               isLoading.set(false)
            }

            override fun onFailure(call: Call<Response<Match>>?, t: Throwable?) {
               handleFailure(t)
               if(t is SocketTimeoutException){
                    //..
               }
            }
        })
    }

    private fun handleError( errorBody: ResponseBody){
        val error = Gson().fromJson(errorBody.charStream(), ErrorBody::class.java)
        Log.e("error", error.error?.message ?: "unknown error")
        isLoading.set(false)
    }

    private fun handleFailure(t: Throwable?){

    }
}