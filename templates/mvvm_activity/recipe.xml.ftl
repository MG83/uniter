<?xml version="1.0"?>
<recipe>

    <instantiate from="src/app_package/ui/activity/MvvmActivity.kt.ftl"
                 to="${escapeXmlAttribute(srcOut)}/ui/activity/${className}Activity.kt" />
    <instantiate from="src/app_package/vm/MvvmViewModel.kt.ftl"
                 to="${escapeXmlAttribute(srcOut)}/vm/${className}ViewModel.kt" />


    <open file="${escapeXmlAttribute(srcOut)}/ui/activity/${className}Activity.kt"/>


    <instantiate from="res/layout/activity_blank.xml.ftl"
                 to="${escapeXmlAttribute(resOut)}/layout/activity_${classToResource(className)}.xml" />

    <open file="${escapeXmlAttribute(resOut)}/layout/activity_${classToResource(className)}.xml" />


    <merge from="AndroidManifest.xml.ftl" to="${escapeXmlAttribute(manifestOut)}/AndroidManifest.xml" />

</recipe>