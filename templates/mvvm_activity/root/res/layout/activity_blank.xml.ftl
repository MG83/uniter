<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools">

    <data>
        <import type="android.view.View"/>
        <!--<import type="me.tatarka.bindingcollectionadapter2.LayoutManagers" />-->
        <variable
                name="viewModel"
                type="${packageName}.vm.${className}ViewModel" />
    </data>

    <LinearLayout
            android:padding="20dp"
            android:orientation="vertical"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            tools:context="${packageName}.ui.activity.${className}Activity">



    </LinearLayout>
</layout>
