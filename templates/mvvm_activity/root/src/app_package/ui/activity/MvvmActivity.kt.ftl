package ${packageName}.ui.activity

import ch.uniter.mvvm.MvvmActivity
import ch.uniter.mvvm.annotation.MvvmLayout

import ${packageName}.R
import ${packageName}.databinding.Activity${className}Binding
import ${packageName}.vm.${className}ViewModel

@MvvmLayout(R.layout.activity_${classToResource(className)})
class ${className}Activity : MvvmActivity< Activity${className}Binding, ${className}ViewModel >(){

}