package ${packageName}.ui.fragment

import ch.uniter.mvvm.MvvmFragment
import ch.uniter.mvvm.annotation.MvvmLayout

import ${packageName}.R
<#if !isShared>
import ${packageName}.vm.${className}ViewModel
<#else>
import ${packageName}.vm.${sharedViewModelName}
</#if>


<#if !isShared>
@MvvmLayout(R.layout.fragment_${classToResource(className)})
class ${className}Fragment : MvvmFragment< Fragment${className}Binding, ${className}ViewModel >(){
<#else>
@MvvmLayout(R.layout.fragment_${classToResource(className)}, isShared=true)
class ${className}Fragment : MvvmFragment< Fragment${className}Binding, ${sharedViewModelName} >(){
</#if>


}