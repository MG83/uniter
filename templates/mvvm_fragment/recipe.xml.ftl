<?xml version="1.0"?>
<recipe>

    <instantiate from="src/app_package/ui/fragment/MvvmFragment.kt.ftl"
                 to="${escapeXmlAttribute(srcOut)}/ui/fragment/${className}Fragment.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/ui/fragment/${className}Fragment.kt"/>

<#if !isShared>
    <instantiate from="src/app_package/vm/MvvmViewModel.kt.ftl"
                 to="${escapeXmlAttribute(srcOut)}/vm/${className}ViewModel.kt" />
    <open file="${escapeXmlAttribute(srcOut)}/vm/${className}ViewModel.kt"/>
</#if>



    <instantiate from="res/layout/fragment_blank.xml.ftl"
                 to="${escapeXmlAttribute(resOut)}/layout/fragment_${classToResource(className)}.xml" />
    <open file="${escapeXmlAttribute(resOut)}/layout/fragment_${classToResource(className)}.xml" />



</recipe>