package ch.uniter.mvvm

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.util.Log
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import java.lang.reflect.ParameterizedType
import ch.uniter.mvvm.annotation.MvvmLayout

/**
 * Created by Marian on 29.06.17.
 */
class Mvvm< VM : MvvmViewModel, B: ViewDataBinding>(val lifecycle: Lifecycle) : LifecycleObserver {

    private var bindingCreated = false
    var viewModelConfig:ViewModelBindingConfig<VM>? = null

    var viewModel: VM? = null
    var binding: B? = null
    var firstAttachment = true

    fun onCreate(view: ViewInterface<B, VM>, savedInstanceState: Bundle?) {

        if (viewModelConfig == null)
            throw IllegalStateException("MvvmViewModel has not been set up. You probably need to call setup() before calling super.onCreate().")

        // skip if already created
        if (bindingCreated) return

        if(viewModel==null){
            @Suppress("UNCHECKED_CAST")
            viewModel = ViewModelResolver.resolve(view, viewModelConfig!!) as VM
            viewModel!!.handleViewModelCreate()
        }

        // perform Data Binding initialization

        if (view is Activity)
            binding = DataBindingUtil.setContentView(view, viewModelConfig!!.layoutResource)
        else if (view is Fragment)
            binding = DataBindingUtil.inflate(LayoutInflater.from(view.context), viewModelConfig!!.layoutResource, null, false)
        else
            throw IllegalArgumentException("View must be an instance of Activity or Fragment (support-v4).")
        bindingCreated = true

        // bind all together
        if(!(viewModelConfig!!.isShared && view is Fragment))
            viewModel!!.view = view

        binding!!.setVariable(viewModelConfig!!.viewModelVariableName!!, viewModel)
        binding!!.setVariable(viewModelConfig!!.viewVariableName!!, view)

        viewModel!!.onViewAttached(firstAttachment)
        firstAttachment = false
    //    viewModel.internalRunAllUiTasksInQueue()
    }

    fun onSaveInstanceState(outState: Bundle?){
        //no completed
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume(){
        Log.d("mvvm","onResume()")
        viewModel?.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause(){
        Log.d("mvvm","onPause()")
        viewModel?.onPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy(){
        Log.d("mvvm","onDestroy()")
        if(!viewModelConfig!!.isSingleton){
            viewModel?.onDestroy()
           // ViewModelResolver.destroy(viewModelConfig!!.viewModelClass as  Class<MvvmViewModel>)
        }
        bindingCreated = false
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        viewModel?.onActivityResult(requestCode,resultCode,data)
    }

    internal fun checkLayoutAnnotation(target:Any){
        val clazz = target.javaClass
        if(!clazz.isAnnotationPresent(MvvmLayout::class.java)){
            Log.d("mvvm","annotation not exist")
            return
        }
        Log.d("mvvm","annotations count: ${clazz.annotations.size}")
        val a = clazz.getAnnotation(MvvmLayout::class.java)
        Log.d("mvvm","checkLayoutAnnotation -> found....")
        setup(a.value, getVmTypeFromClass(target), a.isShared, a.isStatic)
    }

    fun setup(@LayoutRes layoutId: Int,
              viewModelClass: Class<VM>,
              isShared:Boolean = false,
              isStatic:Boolean = false) {

        viewModelConfig = ViewModelBindingConfig(layoutId, viewModelClass, isShared, isStatic)
        lifecycle.addObserver(this)
    }

    @Suppress("UNCHECKED_CAST")
    private fun getVmTypeFromClass(clazz:Any): Class<VM>{
        return (clazz.javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<VM>
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray){
        viewModel?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}