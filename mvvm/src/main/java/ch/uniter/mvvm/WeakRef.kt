package ch.uniter.mvvm

import java.lang.ref.WeakReference
import kotlin.reflect.KProperty

/**
 * Created by Marian on 18.10.17.
 */

class WeakRef<T>(private var _value:WeakReference<T?>) {
    operator fun getValue(thisRef:Any?, property: KProperty<*>): T?{
        return _value.get()
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T?){
        _value = WeakReference(value)
    }
}

@Suppress("NOTHING_TO_INLINE")
inline fun <T> weakRef(value: T) = WeakRef(WeakReference(value))

//example -> var updateCallback: SomeListener? by weakRef(null)