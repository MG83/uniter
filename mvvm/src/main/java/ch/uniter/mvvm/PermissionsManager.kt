package ch.uniter.mvvm

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

import java.util.HashMap

/**
 * Created by Marian on 31.07.17.
 */

class PermissionsManager internal constructor(private var viewModel: MvvmViewModel) {

    private var onPermissionResultCallback:((permissionsResult: PermissionsManager.PermissionsResult)->Unit)? = null

    fun checkOrRequestPermissions(permission: String, onPermissionResult:(permissionsResult: PermissionsManager.PermissionsResult)->Unit) {
        checkOrRequestPermissions(arrayOf(permission), onPermissionResult)
    }

    fun checkOrRequestPermissions(permissions: Array<String>, onPermissionResult:(permissionsResult: PermissionsManager.PermissionsResult)->Unit) {
        val result = checkPermissions(*permissions)
        if (result.isGranted)
            onPermissionResult.invoke(result)
        else {
            requestPermissions(permissions, onPermissionResult)
        }
    }

    fun shouldShowRequestPermissionRationale(permission: String): Boolean {
        return if (viewModel.view is Fragment) {
            (viewModel.view as Fragment).shouldShowRequestPermissionRationale(permission)
        } else {
            ActivityCompat.shouldShowRequestPermissionRationale(viewModel.view!!.mvvmActivity!!, permission)
        }
    }

    fun checkPermissions(vararg permissions: String): PermissionsResult {
        val results = HashMap<String, Boolean>()

        for (permission in permissions) {
            val result = ContextCompat.checkSelfPermission(viewModel.view!!.getContext().applicationContext, permission)
            results.put(permission, result == PackageManager.PERMISSION_GRANTED)
        }
        return PermissionsResult(results)
    }

    fun requestPermissions(permission: String, onPermissionResult:(permissionsResult: PermissionsManager.PermissionsResult)->Unit) {
        requestPermissions(arrayOf(permission), onPermissionResult)
    }

    fun requestPermissions(permissions: Array<String>,  onPermissionResult:(permissionsResult: PermissionsManager.PermissionsResult)->Unit) {
        this.onPermissionResultCallback = onPermissionResult
        if (viewModel.view is Fragment) {
            (viewModel.view as Fragment).requestPermissions(permissions, REQUEST_PERMISSIONS)
        } else {
            ActivityCompat.requestPermissions(viewModel.view!!.mvvmActivity!!, permissions, REQUEST_PERMISSIONS)
        }
    }

    val applicationPermissionSettingsIntent: Intent
        get() {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", viewModel.view!!.getContext().applicationContext.packageName, null)
            intent.data = uri
            return intent
        }


    internal fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_PERMISSIONS) {
            onPermissionResultCallback?.invoke(PermissionsResult(permissions, grantResults))
            onPermissionResultCallback = null
        }
    }

    inner class PermissionsResult {
        internal var result: MutableMap<String, Boolean>

        constructor(result: MutableMap<String, Boolean>) {
            this.result = result
        }

        constructor(permissions: Array<String>, grantResults: IntArray) {
            result = HashMap()
            for (i in permissions.indices) {
                result.put(permissions[i], grantResults[i] == PackageManager.PERMISSION_GRANTED)
            }
        }

        val isGranted: Boolean
            get(){
                for (p in result) if(!p.value)return false
                return true
            }

        val resultsMap: Map<String, Boolean>
            get() = result
    }

    companion object {
        val REQUEST_PERMISSIONS = 62538
    }
}
