package ch.uniter.mvvm.annotation

import androidx.annotation.LayoutRes


/**
 * Created by gim on 15.10.17.
 */

@Retention
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE) // class level
annotation class MvvmLayout constructor(@LayoutRes val value: Int, val isShared: Boolean = false, val isStatic: Boolean = false)
