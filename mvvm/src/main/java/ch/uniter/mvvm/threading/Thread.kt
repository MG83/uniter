package ch.uniter.mvvm.threading

import android.os.Handler
import android.os.Looper

/**
 * Created by Marian on 12.11.17.
 */

fun async(action: () -> Unit) = Thread(Runnable(action)).start()

inline fun uiThread(crossinline action: () -> Unit) {
    if (isMainLooperAlive()) {
        action()
    } else {
        Handler(Looper.getMainLooper()).post{action.invoke()}
    }
}

fun asyncDelay(delayMillis: Long, action: () -> Unit) = Handler().postDelayed(Runnable(action), delayMillis)

fun uiThreadDelay(delayMillis: Long, action: () -> Unit) = Handler(Looper.getMainLooper()).postDelayed(Runnable(action), delayMillis)

fun isMainLooperAlive() = Looper.myLooper() == Looper.getMainLooper()