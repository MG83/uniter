package ch.uniter.mvvm

import android.content.Intent
import android.util.Log
import androidx.annotation.CallSuper
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel


/**
 * Created by YooApps on 29.06.17.
 */
abstract class MvvmViewModel : ViewModel(){

    val permission: PermissionsManager by lazy { PermissionsManager(this) }

    var view: ViewInterface<*, out MvvmViewModel>? = null
    internal var isCreated:Boolean = false

    val activity: FragmentActivity?
        get() = view?.mvvmActivity

    @CallSuper
    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        Log.d("mvvvmViewModel","onActivityResult -> resultCode:$resultCode")
    }

    @CallSuper
    open fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray){
        permission.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    open fun onViewModelCreated(){
        Log.d("MvvmViewModel","onViewModelCreated()")
    }

    open fun onResume(){
        Log.d("MvvmViewModel","onResume()")
    }

    open fun onPause(){
        Log.d("MvvmViewModel","onPause()")
    }

    open fun onDestroy() {
        Log.d("MvvmViewModel","onDestroy()")
    }

    open fun onViewAttached(firstAttachment: Boolean) {

    }

    internal fun handleViewModelCreate(){
        if(isCreated){
            onResume()
            return
        }
        onViewModelCreated()
        isCreated = true
    }

    val hasViewAttached: Boolean
        get() = view != null


}