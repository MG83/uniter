package ch.uniter.mvvm

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.lang.reflect.ParameterizedType


/**
 * Created by YooApps on 31.07.17.
 */

abstract class MvvmBottomSheetDialogFragment<T : ViewDataBinding, S : MvvmViewModel> : BottomSheetDialogFragment()
        , ViewInterface<T, S> {

    protected val mvvm = Mvvm<S, T>(lifecycle)

    init{ mvvm.checkLayoutAnnotation(this) }

    var bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if(newState == BottomSheetBehavior.STATE_HIDDEN){
                dismiss()
            }
        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        mvvm.onCreate(this,null)
        val view = mvvm.binding!!.root
        dialog.setContentView(view)
        BottomSheetBehavior.from(view.parent as View)?.setBottomSheetCallback(bottomSheetCallback)
    }

    override fun show(manager: FragmentManager, tag: String?) {
        val transaction = manager.beginTransaction()
        val prevFragment = manager.findFragmentByTag(tag)

        if (prevFragment != null) {
            transaction.remove(prevFragment)
            transaction.addToBackStack(null)
            Log.w("MvvmBottomSheetDialog","call dismiss() instead of dialog.hide()")
        }

        super.show(transaction, tag)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(context, theme)
    }

    override fun getContext(): Context {
        return this.activity!!
    }

    override val mvvmActivity: FragmentActivity?
        get() = this.activity

    override val bundle: Bundle?
        get() = arguments

    var viewModel: S?
        get() { return mvvm.viewModel }
        set(value) {
            mvvm.viewModel = value}

    override val binding: T
        get() = mvvm.binding as T

    override fun onSaveInstanceState(outState: Bundle) {
        mvvm.onSaveInstanceState(outState)
        super.onSaveInstanceState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mvvm.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mvvm.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    protected fun setup(@LayoutRes layoutResourceId: Int, isShared:Boolean = false, isStatic:Boolean = false) {
        @Suppress("UNCHECKED_CAST")
        val clazz =  (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<S>
        setup(layoutResourceId, clazz, isShared, isStatic)
    }

    protected fun setup(@LayoutRes layoutResourceId: Int, viewModelClass: Class<S>, isShared:Boolean = false, isStatic:Boolean = false) {
        Log.d("MvvmBottomSheetDialog","setup  fragment for -> "+viewModelClass.simpleName)
        @Suppress("UNCHECKED_CAST")
        mvvm.setup(layoutResourceId, viewModelClass, isShared, isStatic)
    }
}
