package ch.uniter.mvvm

import android.util.Log
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider

/**
 * Created by gim on 04.10.17.
 */
object ViewModelResolver {

    private val viewModelCache:MutableMap<Class<MvvmViewModel>, MvvmViewModel> = mutableMapOf()

    @JvmStatic
    fun  <B: ViewDataBinding, VM:MvvmViewModel> resolve(view: ViewInterface<B, VM>, viewModelConfig: ViewModelBindingConfig<VM>): MvvmViewModel?
    {
        @Suppress("UNCHECKED_CAST")
        val clazz: Class<MvvmViewModel> = viewModelConfig.viewModelClass as  Class<MvvmViewModel>
        Log.d("test","1. clazz: ${clazz.canonicalName}")

        if(viewModelConfig.isSingleton)
            return resolve(clazz)

        if(view is FragmentActivity)
            return resolve(view as FragmentActivity, clazz)

        if(view is Fragment) {
            if (viewModelConfig.isShared) {
                return resolve(view.mvvmActivity!!, clazz)
            }
            return resolve(view as Fragment, clazz)
        }

        throw IllegalArgumentException("View must be an instance of Activity or Fragment (support-v4).")
    }

    @JvmStatic
    fun resolve(clazz: Class<MvvmViewModel>): MvvmViewModel{
        Log.d("test","clazz: ${clazz.canonicalName}")
        if(viewModelCache.contains(clazz)) return viewModelCache[clazz]!!
        val vm =  clazz.newInstance()
        viewModelCache.put(clazz,vm)
        return vm
    }

    @JvmStatic
    fun resolve(activity:FragmentActivity, clazz: Class<MvvmViewModel>): MvvmViewModel{
        return ViewModelProvider(activity).get(clazz)
    }

    @JvmStatic
    fun resolve(fragment:Fragment, clazz: Class<MvvmViewModel>): MvvmViewModel{
        return ViewModelProvider(fragment).get(clazz)
    }

    @JvmStatic
    fun addOrReplace(viewModel:MvvmViewModel): MvvmViewModel{
        Log.d("test","clazz: ${viewModel.javaClass.canonicalName}")
        if(viewModelCache.contains( viewModel.javaClass)){
            viewModelCache.remove(viewModel.javaClass)
        }
        viewModelCache.put(viewModel.javaClass,viewModel)

        return viewModel
    }

    @JvmStatic
    fun remove(clazz: Class<MvvmViewModel>){
        if(viewModelCache.containsKey(clazz)) viewModelCache.remove(clazz)
    }

    @JvmStatic
    fun removeAll(){
       for (vm in viewModelCache){
           viewModelCache.remove(vm.key)
       }
    }
}