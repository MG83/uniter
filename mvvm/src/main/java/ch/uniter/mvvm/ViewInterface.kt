package ch.uniter.mvvm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity

/**
 * Created by YooApps on 29.06.17.
 */

interface ViewInterface<B: ViewDataBinding, M : MvvmViewModel> {
    val binding: B
    fun getContext(): Context
    val mvvmActivity: FragmentActivity?
    val bundle: Bundle?
    fun startActivityForResult(intent: Intent?, requestCode: Int)
    fun startActivityForResult(intent: Intent?, requestCode: Int, bundle: Bundle?)
}
