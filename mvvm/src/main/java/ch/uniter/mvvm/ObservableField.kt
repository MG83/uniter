package ch.uniter.mvvm

import androidx.databinding.ObservableField


/**
 * Created by gim on 12.11.17.
 */

var <T>ObservableField<T>.value
    get() = this.get()
    set(v) { this.set(v) }
