package ch.uniter.mvvm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import java.lang.reflect.ParameterizedType
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity


/**
 * Created by YooApps on 31.07.17.
 */

abstract class MvvmFragment<T : ViewDataBinding, S : MvvmViewModel> : Fragment()
        , ViewInterface<T, S> {

    private val mvvm = Mvvm<S, T>(lifecycle)

    init{
        mvvm.checkLayoutAnnotation(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        mvvm.onCreate(this, savedInstanceState)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mvvm.onCreate(this, savedInstanceState)
        return mvvm.binding!!.root
    }

    override fun getContext(): Context {
        return this.activity!!
    }

    override val mvvmActivity: FragmentActivity?
        get() = this.activity

    override val bundle: Bundle?
        get() = arguments

    var viewModel: S?
        get() { return mvvm.viewModel }
        set(value) {
            mvvm.viewModel = value}

    override val binding: T
        get() = mvvm.binding as T

    override fun onSaveInstanceState(outState: Bundle) {
        mvvm.onSaveInstanceState(outState)
        super.onSaveInstanceState(outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mvvm.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mvvm.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    protected fun setup(@LayoutRes layoutResourceId: Int, isShared:Boolean = false, isStatic:Boolean = false) {
        @Suppress("UNCHECKED_CAST")
        val clazz =  (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<S>
        setup(layoutResourceId, clazz, isShared, isStatic)
    }

    protected fun setup(@LayoutRes layoutResourceId: Int, viewModelClass: Class<S>, isShared:Boolean = false, isStatic:Boolean = false) {
        Log.d("MvvmFragment","setup  fragment -> "+viewModelClass.simpleName)
        @Suppress("UNCHECKED_CAST")
        mvvm.setup(layoutResourceId, viewModelClass, isShared, isStatic)
    }
}
