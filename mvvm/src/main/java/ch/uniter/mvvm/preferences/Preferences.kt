package ch.uniter.mvvm.preferences

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by YooApps on 29.06.17.
 */

class Preferences(private val context: Context, val key:String="key" ) {

    val shared: SharedPreferences by lazy { context.getSharedPreferences(key, Context.MODE_PRIVATE) }

    fun register(listener:SharedPreferences.OnSharedPreferenceChangeListener){
        shared.registerOnSharedPreferenceChangeListener(listener)
    }

    fun unregister(listener:SharedPreferences.OnSharedPreferenceChangeListener){
        shared.unregisterOnSharedPreferenceChangeListener(listener)
    }
}