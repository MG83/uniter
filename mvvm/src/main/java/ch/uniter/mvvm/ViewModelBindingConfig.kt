package ch.uniter.mvvm


/**
 * Created by Marian on 29.06.17.
 */

data class ViewModelBindingConfig<VM:MvvmViewModel>
@JvmOverloads constructor(var layoutResource: Int,
      var viewModelClass: Class<VM>,
      var isShared: Boolean = false,
      var isSingleton: Boolean = false,
      var viewModelVariableName: Int? = ch.uniter.mvvm.BR.viewModel,
      var viewVariableName: Int? = ch.uniter.mvvm.BR.view
)
