package ch.uniter.mvvm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import java.lang.reflect.ParameterizedType


/**
 * Created by YooApps on 29.06.17.
 */
abstract class MvvmActivity<B: ViewDataBinding, M : MvvmViewModel>: AppCompatActivity()
        , ViewInterface<B, M> {

    private val mvvm = Mvvm<M,B>(lifecycle)

    init{
        mvvm.checkLayoutAnnotation(this)
    }

    var viewModel: M?
        get() { return mvvm.viewModel }
        set(value) {
            mvvm.viewModel = value}

    override val binding: B
        get() {return mvvm.binding as B}

    override val mvvmActivity: AppCompatActivity
        get() {return this}

    override val bundle: Bundle?
        get() {return intent?.extras}

    override fun getContext(): Context {
        return this
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        mvvm.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mvvm.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mvvm.onCreate(this, savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        mvvm.onCreate(this, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mvvm.onSaveInstanceState(outState)
    }

    protected fun setup(@LayoutRes layoutResourceId: Int, isStatic: Boolean = false) {
        @Suppress("UNCHECKED_CAST")
        val clazz =  (javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<M>
        setup(layoutResourceId, clazz, isStatic)
    }

    protected fun setup(@LayoutRes layoutResourceId: Int, viewModelClass:Class<M>, isStatic: Boolean=false) {
        Log.d("MvvmActivity","setup activity -> "+viewModelClass.simpleName)
        mvvm.setup(layoutResourceId, viewModelClass, false, isStatic)
    }
}