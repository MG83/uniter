package ch.uniter.validation

import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding

import java.util.HashSet

import ch.uniter.validation.rule.Rule
import ch.uniter.validation.util.ViewTagHelper

/**
 * Created by YooApps on 02.10.17.
 */

class Validator(private val target: ViewDataBinding) {
    private var validationListener: ValidationListener? = null

    private var mode = FIELD_VALIDATION_MODE
    private val disabledViews: MutableSet<View>

    init {
        this.disabledViews = HashSet()
    }

    fun setValidationListener(validationListener: ValidationListener) {
        this.validationListener = validationListener
    }

    fun toValidate() {
        if (validationListener == null) throw IllegalArgumentException("Validation listener should not be null.")

        if (validate()) {
            validationListener!!.onValidationSuccess()
        } else {
            validationListener!!.onValidationError()
        }
    }

    fun validate(): Boolean {
        val viewWithValidations = viewsWithValidation
        return isAllViewsValid(viewWithValidations)
    }

    fun validate(view: View): Boolean {
        val viewWithValidations = getViewsWithValidation(view)
        return isAllViewsValid(viewWithValidations)
    }

    fun <ViewType : View> validate(views: List<ViewType>): Boolean {
        val viewWithValidations = getViewsWithValidation(views)
        return isAllViewsValid(viewWithValidations)
    }

    private fun isAllViewsValid(viewWithValidations: List<View>): Boolean {
        var allViewsValid = true
        for (viewWithValidation in viewWithValidations) {
            var viewValid = true
            val rules = viewWithValidation.getTag(R.id.validator_rule) as List<Rule<*,*>>
            for (rule in rules) {
                viewValid = viewValid && isRuleValid(rule)
                allViewsValid = allViewsValid && viewValid
            }

            if (mode == FIELD_VALIDATION_MODE && !viewValid) {
                break
            }
        }
        return allViewsValid
    }

    private fun isRuleValid(rule:Rule<*,*>): Boolean {
        return disabledViews.contains(rule.view) || rule.validate()
    }

    fun disableValidation(view: View) {
        disabledViews.add(view)
    }

    fun enableValidation(view: View) {
        disabledViews.remove(view)
    }

    fun enableFormValidationMode() {
        this.mode = FORM_VALIDATION_MODE
    }

    fun enableFieldValidationMode() {
        this.mode = FIELD_VALIDATION_MODE
    }

    private val viewsWithValidation: List<View>
        get() = if (target.root is ViewGroup) {
            ViewTagHelper.getViewsByTag(target.root as ViewGroup, R.id.validator_rule)
        } else {
            listOf(target.root)
        }

    private fun <ViewType : View> getViewsWithValidation(views: List<ViewType>): List<View> {
        return ViewTagHelper.filterViewsWithTag(R.id.validator_rule, views)
    }

    private fun getViewsWithValidation(view: View): List<View> {
        return ViewTagHelper.filterViewWithTag(R.id.validator_rule, view)
    }

    interface ValidationListener {

        fun onValidationSuccess()

        fun onValidationError()
    }

    companion object {
        private val FIELD_VALIDATION_MODE = 0
        private val FORM_VALIDATION_MODE = 1
    }
}
