package ch.uniter.validation.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter

import ch.uniter.validation.R
import ch.uniter.validation.rule.TypeRule
import ch.uniter.validation.util.EditTextHandler
import ch.uniter.validation.util.ErrorMessageHelper
import ch.uniter.validation.util.ViewTagHelper

/**
 * Created by YooApps on 02.10.17.
 */

object TypeBindings {
    @JvmStatic
    @BindingAdapter("app:validateType", "app:validateTypeMessage", "app:validateTypeAutoDismiss", requireAll = false)
    fun bindingTypeValidation(view: TextView, fieldTypeText: String?, errorMessage: String?, autoDismiss: Boolean = true) {
        if (autoDismiss) {
            EditTextHandler.disableErrorOnChanged(view)
        }
        if(fieldTypeText==null)
            return
        try {
            val fieldType = getFieldTypeByText(fieldTypeText)
            val handledErrorMessage = ErrorMessageHelper.getStringOrDefault(view,
                    errorMessage, fieldType.errorMessageId)
            ViewTagHelper.appendValue(R.id.validator_rule, view, fieldType.instantiate(view, handledErrorMessage))
        } catch (ignored: Exception) {
        }
    }

    @JvmStatic
    private fun getFieldTypeByText(fieldTypeText: String?): TypeRule.FieldType {
        if(fieldTypeText==null) return TypeRule.FieldType.None
        return TypeRule.FieldType.values().firstOrNull { it.toString().equals(fieldTypeText, ignoreCase = true) }
                ?: TypeRule.FieldType.None
    }
}
