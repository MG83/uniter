package ch.uniter.validation.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter

import ch.uniter.validation.R
import ch.uniter.validation.rule.RegexRule
import ch.uniter.validation.util.EditTextHandler
import ch.uniter.validation.util.ErrorMessageHelper
import ch.uniter.validation.util.ViewTagHelper

/**
 * Created by YooApps on 02.10.17.
 */

object RegexBindings {

    @JvmStatic
    @BindingAdapter("validateRegex", "validateRegexMessage", "validateRegexAutoDismiss", requireAll = false)
    fun bindingRegex(view: TextView, pattern: String, errorMessage: String, autoDismiss: Boolean = true) {
        if (autoDismiss) {
            EditTextHandler.disableErrorOnChanged(view)
        }

        val handledErrorMessage = ErrorMessageHelper.getStringOrDefault(view,
                errorMessage, R.string.error_message_regex_validation)
        ViewTagHelper.appendValue(R.id.validator_rule, view, RegexRule(view, pattern, handledErrorMessage))
    }

}