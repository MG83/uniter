package ch.uniter.validation.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter

import ch.uniter.validation.R
import ch.uniter.validation.rule.ConfirmPasswordRule
import ch.uniter.validation.util.EditTextHandler
import ch.uniter.validation.util.ErrorMessageHelper
import ch.uniter.validation.util.ViewTagHelper

/**
 * Created by YooApps on 02.10.17.
 */

object PasswordBindings {

    @JvmStatic
    @BindingAdapter("validatePassword", "validatePasswordMessage", "validatePasswordAutoDismiss", requireAll = false)
    fun bindingPassword(view: TextView, comparableView: TextView, errorMessage: String, autoDismiss: Boolean = true) {
        if (autoDismiss) {
            EditTextHandler.disableErrorOnChanged(view)
        }

        val handledErrorMessage = ErrorMessageHelper.getStringOrDefault(view,
                errorMessage, R.string.error_message_password_not_equal)
        ViewTagHelper.appendValue(R.id.validator_rule, view,
                ConfirmPasswordRule(view, comparableView, handledErrorMessage))
    }

}
