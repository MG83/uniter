package ch.uniter.validation.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter

import ch.uniter.validation.R
import ch.uniter.validation.rule.EmptyRule
import ch.uniter.validation.rule.MaxLengthRule
import ch.uniter.validation.rule.MinLengthRule
import ch.uniter.validation.util.EditTextHandler
import ch.uniter.validation.util.ErrorMessageHelper
import ch.uniter.validation.util.ViewTagHelper

/**
 * Created by YooApps on 02.10.17.
 */

object LengthBindings {

    @JvmStatic
    @BindingAdapter("validateMinLength", "validateMinLengthMessage", "validateMinLengthAutoDismiss", requireAll = false)
    fun bindingMinLength(view: TextView, minLength: Int, errorMessage: String, autoDismiss: Boolean) {
        if (autoDismiss) {
            EditTextHandler.disableErrorOnChanged(view)
        }

        val handledErrorMessage = ErrorMessageHelper.getStringOrDefault(view,
                errorMessage, R.string.error_message_min_length, minLength)
        ViewTagHelper.appendValue(R.id.validator_rule, view, MinLengthRule(view, minLength, handledErrorMessage))
    }

    @JvmStatic
    @BindingAdapter("validateMaxLength", "validateMaxLengthMessage", "validateMaxLengthAutoDismiss", requireAll = false)
    fun bindingMaxLength(view: TextView, maxLength: Int, errorMessage: String, autoDismiss: Boolean) {
        if (autoDismiss) {
            EditTextHandler.disableErrorOnChanged(view)
        }

        val handledErrorMessage = ErrorMessageHelper.getStringOrDefault(view,
                errorMessage, R.string.error_message_max_length, maxLength)
        ViewTagHelper.appendValue(R.id.validator_rule, view, MaxLengthRule(view, maxLength, handledErrorMessage))
    }

    @JvmStatic
    @BindingAdapter("validateEmpty", "validateEmptyMessage", "validateEmptyAutoDismiss", requireAll = false)
    fun bindingEmpty(view: TextView, empty: Boolean, errorMessage: String, autoDismiss: Boolean) {
        if (autoDismiss) {
            EditTextHandler.disableErrorOnChanged(view)
        }

        val handledErrorMessage = ErrorMessageHelper.getStringOrDefault(view,
                errorMessage, R.string.error_message_empty_validation)
        ViewTagHelper.appendValue(R.id.validator_rule, view, EmptyRule(view, empty, handledErrorMessage))
    }

}
