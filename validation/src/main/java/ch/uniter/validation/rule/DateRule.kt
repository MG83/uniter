package ch.uniter.validation.rule

import android.widget.TextView

import ch.uniter.validation.util.EditTextHandler

/**
 * Created by Marian on 02.10.17.
 */

class DateRule(view: TextView, value: String, errorMessage: String) : Rule<TextView, String>(view, value, errorMessage) {

    public override fun isValid(view: TextView): Boolean {
        val matcher = DATE_PATTERN.toPattern().matcher(view.text.toString())
        return matcher.matches()
    }

    public override fun onValidationSucceeded(view: TextView) {
        EditTextHandler.removeError(view)
    }

    public override fun onValidationFailed(view: TextView) {
        EditTextHandler.setError(view, errorMessage)
    }

    companion object {
        private val DATE_PATTERN = "(0?[1-9]|1[012]) [/.-] (0?[1-9]|[12][0-9]|3[01]) [/.-] ((19|20)\\d\\d)"
    }

}
