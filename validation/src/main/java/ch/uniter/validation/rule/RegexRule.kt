package ch.uniter.validation.rule

import android.widget.TextView

import ch.uniter.validation.util.EditTextHandler

/**
 * Created by Marian on 02.10.17.
 */

class RegexRule(view: TextView, value: String, errorMessage: String) : Rule<TextView, String>(view, value, errorMessage) {

    public override fun isValid(view: TextView): Boolean {
        return view.text.toString().matches(value.toRegex())
    }

    public override fun onValidationSucceeded(view: TextView) {
        EditTextHandler.removeError(view)
    }

    public override fun onValidationFailed(view: TextView) {
        EditTextHandler.setError(view, errorMessage)
    }
}