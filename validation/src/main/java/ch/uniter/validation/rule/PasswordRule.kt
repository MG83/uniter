package ch.uniter.validation.rule

import android.util.Log
import android.widget.TextView
import ch.uniter.validation.R
import ch.uniter.validation.util.EditTextHandler

/**
 * Created by Marian on 3/24/17.
 */
class PasswordRule(view: TextView, errorMessage: String) : TypeRule(view, FieldType.Password, errorMessage) {

    /**
        At least one upper case English letter, (?=.*?[A-Z])
        At least one lower case English letter, (?=.*?[a-z])
        At least one digit, (?=.*?[0-9])
        At least one special character, (?=.*?[#?!@$%^&*-])
        Minimum eight in length .{8,} (with the anchors)
     */

    override fun isValid(view: TextView): Boolean {
        val password = view.text.toString()
        Log.d("validate","password=$password")
        if(password.isEmpty()) {
            errorMessage = view.context.getString(R.string.error_message_password_validation)
            return false
        }

        val hasOneUpperCaseLetter =  password.matches(".*[A-Z].*".toRegex())
        Log.d("validate","hasOneUpperCaseLetter=$hasOneUpperCaseLetter")
        errorMessage = view.context.getString(R.string.error_message_password_one_uppercase)
        if(!hasOneUpperCaseLetter)return false

        val hasOneLowerCaseLetter =   password.matches(".*[a-z].*".toRegex())
        Log.d("validate","hasOneLowerCaseLetter=$hasOneLowerCaseLetter")
        errorMessage = view.context.getString(R.string.error_message_password_one_lowercase)
        if(!hasOneLowerCaseLetter)return false

        val hasOneDigit = password.matches(".*[0-9].*".toRegex())
        Log.d("validate","hasOneDigit=$hasOneDigit")
        errorMessage = view.context.getString(R.string.error_message_password_one_digit)
        if(!hasOneDigit)return false

        val hasOneSpecialChar = password.matches(".*[#?!@\$%^&*-].*".toRegex())
        Log.d("validate","hasOneSpecialChar=$hasOneSpecialChar")
        errorMessage = view.context.getString(R.string.error_message_password_one_special_character)
        if(!hasOneSpecialChar)return false

        val min8Length = password.length >= 8
        Log.d("validate","min8Length=$min8Length")
        errorMessage = view.context.getString(R.string.error_message_password_not_long_enough)
        if(!min8Length)return false

        return true
    }

    override fun onValidationSucceeded(view: TextView) {
        super.onValidationSucceeded(view)
        EditTextHandler.removeError(view)
    }

    override fun onValidationFailed(view: TextView) {
        super.onValidationFailed(view)
        EditTextHandler.setError(view, errorMessage)
    }
}