package ch.uniter.validation.rule

import android.util.Patterns
import android.widget.TextView

import ch.uniter.validation.util.EditTextHandler

/**
 * Created by Marian on 30.09.17.
 */

class EmailTypeRule(view: TextView, errorMessage: String) : TypeRule(view, TypeRule.FieldType.Email, errorMessage) {

    override fun isValid(view: TextView): Boolean {
        val emailPattern = Patterns.EMAIL_ADDRESS
        return emailPattern.matcher(view.text).matches()
    }

    override fun onValidationSucceeded(view: TextView) {
        super.onValidationSucceeded(view)
        EditTextHandler.removeError(view)
    }

    override fun onValidationFailed(view: TextView) {
        super.onValidationFailed(view)
        EditTextHandler.setError(view, errorMessage)
    }
}