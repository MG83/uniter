package ch.uniter.validation.rule

/**
 * Created by YooApps on 02.10.17.
 */


import android.widget.TextView

import ch.uniter.validation.util.EditTextHandler

/**
 * Created by Marian on 3/24/17.
 */
class UsernameRule(view: TextView, errorMessage: String) : TypeRule(view, TypeRule.FieldType.Username, errorMessage) {

    override fun isValid(view: TextView): Boolean {
        val username = view.text.toString()
        return username.matches("[a-zA-Z0-9-._]+".toRegex())
    }

    override fun onValidationSucceeded(view: TextView) {
        super.onValidationSucceeded(view)
        EditTextHandler.removeError(view)
    }

    override fun onValidationFailed(view: TextView) {
        super.onValidationFailed(view)
        EditTextHandler.setError(view, errorMessage)
    }
}
