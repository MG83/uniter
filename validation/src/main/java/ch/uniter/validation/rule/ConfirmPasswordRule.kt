package ch.uniter.validation.rule

import android.widget.TextView

import ch.uniter.validation.util.EditTextHandler

/**
 * Created by Marian on 02.10.17.
 */

class ConfirmPasswordRule(view: TextView, value: TextView, errorMessage: String) : Rule<TextView, TextView>(view, value, errorMessage) {

    public override fun isValid(view: TextView): Boolean {
        val value1 = view.text.toString()
        val value2 = value.text.toString()
        return value1.trim { it <= ' ' } == value2.trim { it <= ' ' }
    }

    public override fun onValidationSucceeded(view: TextView) {
        EditTextHandler.removeError(view)
    }

    public override fun onValidationFailed(view: TextView) {
        EditTextHandler.setError(view, errorMessage)
    }
}
