package ch.uniter.validation.rule

import android.view.View

/**
 * Created by Marian on 30.09.17.
 */

abstract class Rule<ViewType : View, ValueType>(view: ViewType, var value: ValueType, var errorMessage: String) {
    var view: ViewType
        protected set

    init {
        this.view = view
    }

    fun validate(): Boolean {
        val valid = isValid(view)
        if (valid) {
            onValidationSucceeded(view)
        } else {
            onValidationFailed(view)
        }
        return valid
    }

    protected abstract fun isValid(view: ViewType): Boolean

    protected open fun onValidationSucceeded(view: ViewType) {}

    protected open fun onValidationFailed(view: ViewType) {}
}