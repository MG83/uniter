package ch.uniter.extension
import android.util.Log

/**
 * Utility functions for all classes
 */


/**
 * Returns class name. Useful for Log Tags
 */
val Any.TAG: String
    get() = this::class.java.simpleName

fun Any.logD(message:String, tag: String = TAG) = Log.d(tag, message)
fun Any.logI(message:String, tag: String = TAG) = Log.i(tag, message)
fun Any.logV(message:String, tag: String = TAG) = Log.v(tag, message)
fun Any.logW(message:String, tag: String = TAG) = Log.w(tag, message)
fun Any.logE(message:String, tag: String = TAG) = Log.e(tag, message)
fun Any.logWtf(message:String, tag: String = TAG) = Log.wtf(tag, message)