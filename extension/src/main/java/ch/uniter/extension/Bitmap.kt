package ch.uniter.extension

import android.graphics.Bitmap

/**
 * Created by gim on 12.11.17.
 */
inline fun <T : Bitmap?, R> T.use(block: (T) -> R): R {
    var recycled = false
    try {
        return block(this)
    } catch (e: Exception) {
        recycled = true
        try {
            this?.recycle()
        } catch (exception: Exception) { }
        throw e
    } finally {
        if (!recycled) {
            this?.recycle()
        }
    }
}