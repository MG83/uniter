package ch.uniter.extension.versions

/**
 * Created by gim on 12.11.17.
 */
class InvalidVersionException(val name: String) : Exception(name)