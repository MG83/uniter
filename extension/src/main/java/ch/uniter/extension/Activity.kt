package ch.uniter.extension

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.AttrRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

/**
 * Created by gim on 12.11.17.
 */
fun Activity.hideKeyboard():Boolean {
    val view = currentFocus
    view?.let {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inputMethodManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
    return false
}

fun Activity.showKeyboard():Boolean {
    val view = currentFocus
    view?.let{
        (getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.showSoftInput(view, 0)
        return true
    }
    return false
}

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.setStatusBarDim(dim:Boolean) {
    window.statusBarColor = if (dim) Color.TRANSPARENT else
        ContextCompat.getColor(this, getThemedResId(R.attr.colorPrimaryDark))
  }

fun Activity.getThemedResId(@AttrRes attr: Int): Int {
    val a = theme.obtainStyledAttributes(intArrayOf(attr))
    val resId = a.getResourceId(0, 0)
    a.recycle()
    return resId
}

var Activity.statusBarColor
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    get() = window.statusBarColor
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    set(value){
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, value)
    }

fun Activity.toggleFullscreen(){
    val attrs = window.attributes
    attrs.flags xor(WindowManager.LayoutParams.FLAG_FULLSCREEN)
    window.attributes = attrs
}

var Activity.fullscreen
    get() = window.attributes.flags.and(WindowManager.LayoutParams.FLAG_FULLSCREEN) != 0
    set(value: Boolean) {
        if(value) {
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        } else{
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            window.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        }
    }

fun Activity.showToolbar() {
    actionBar?.show()
}

fun Activity.hideToolbar() {
    actionBar?.hide()
}

fun AppCompatActivity.showToolbar() {
    supportActionBar?.show()
}

fun AppCompatActivity.hideToolbar() {
    supportActionBar?.hide()
}

